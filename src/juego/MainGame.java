package juego;

import java.awt.Color;

import Core.Field;
import Core.Window;
import enums.Direccion;
import enums.FantasmaColor;
import mapa.Mapa1;
import mapa.UI;
import personajes.Fantasma;
import personajes.Pacman;
import personajes.Spawner;

public class MainGame {
	
	//hay que arreglar el spawner y mejorar la IA fantasmil

	static Field f = new Field();
	static Window w = new Window(f);
	static Pacman pacman = Pacman.instanciar(830, 630, 880, 680, f); 
	//static Pacman ms_pacman = Pacman.instanciar(830, 630, 880, 680, f); 
	
	public static void main(String[] args) throws InterruptedException {
		f.setBackground(Color.black);
		Spawner s = new Spawner(f);
		Mapa1.mapa1(f);
		
		UI.getInstance(800, 525, f);
		
		while (true) {
		
			input();
//			System.out.println(Spawner.getFantasmas());
			f.draw();
			Thread.sleep(30);
		}
		
		
	}

	private static void input() {
		Pacman pacman = Pacman.instanciar(830, 630, 880, 680, f); 
		if (w.getKeyPressed() == 'w') {
			pacman.mover(Direccion.arriba);
		}else if (w.getKeyPressed() == 'd') {
			pacman.mover(Direccion.derecha);
		}else if (w.getKeyPressed() == 'a') {
			pacman.mover(Direccion.izquierda);
		}else if (w.getKeyPressed() == 's') {
			pacman.mover(Direccion.abajo);
		}
		
	}

}
