package personajes;

import java.nio.file.Path;
import java.util.Random;

import Core.Field;
import enums.Direccion;
import Core.PhysicBody;
import Core.Sprite;
import enums.FantasmaColor;
import mapa.Pared;
import mapa.Vidas;

public class Fantasma extends PhysicBody {
	public int cooldown = 0;
	int vida = 3;
	Random r = new Random();
	public boolean vulnerable;
	Direccion ultimaDir;
	boolean dispara = false;

	public Fantasma(int x1, int y1, int x2, int y2, FantasmaColor color, Field f) {
		super("Fantasma", x1, y1, x2, y2, 0, colores(color), f);
		mover(Direccion.arriba);
		movimiento();
		this.vulnerable = false;
		Spawner.setFantasmas(1);
	}

	private void movimiento() {

		int random = r.nextInt(4);
		if (random == 0) {
			mover(Direccion.arriba);
		} else if (random == 1) {
			mover(Direccion.abajo);
		} else if (random == 2) {
			mover(Direccion.izquierda);
		} else if (random == 3) {
			mover(Direccion.derecha);
		}
		cooldown = 0;
	}

	@Override
	public void update() {
		// tp arriba y abajo
		if (this.x2 < 0) {
			this.x1 = 1920;
			this.x2 = 1970;
		} else if (this.x1 > 1920) {
			this.x1 = -50;
			this.x2 = 0;
		}
		// tp izquierda y derecha
		if (this.y2 < 0) {
			this.y1 = 1020;
			this.y2 = 1070;
		} else if (this.y1 > 1020) {
			this.y1 = -50;
			this.y2 = 0;
		}
		cooldown++;
		if (cooldown == 50) {
			movimiento();
		}
		//System.out.println(cooldown);
	}

	public static void comestible(Field f) {
		for (Sprite fanta : f.sprites) {
			if (fanta instanceof Fantasma) {
				((Fantasma) fanta).path = "recursos/imagenes/enemigos/azul.gif";
				((Fantasma) fanta).vulnerable = true;
			}
		}
	}

	private static String colores(FantasmaColor color) {

		if (color == FantasmaColor.ROJO) {
			return "recursos/imagenes/enemigos/rojo.gif";
		} else if (color == FantasmaColor.ROSA) {
			return "recursos/imagenes/enemigos/rosa.gif";
		} else if (color == FantasmaColor.VERDE) {
			return "recursos/imagenes/enemigos/verde.gif";
		} else {
			return "";
		}

	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		int movimiento;
		if (sprite instanceof Pared) {
			movimiento = r.nextInt(4);
			if (movimiento == 0 && ultimaDir != Direccion.arriba) {
				mover(Direccion.arriba);
			} else if (movimiento == 1 && ultimaDir != Direccion.derecha) {
				mover(Direccion.derecha);
			} else if (movimiento == 2 && ultimaDir != Direccion.izquierda) {
				mover(Direccion.izquierda);
			} else if (movimiento == 3 && ultimaDir != Direccion.abajo) {
				mover(Direccion.abajo);
			}
		}
//		else if(sprite instanceof Fantasma) {
//			if (((Fantasma) sprite).ultimaDir == Direccion.derecha) {
//				this.mover(Direccion.derecha);
//				((Fantasma) sprite).mover(Direccion.izquierda);
//			}else if(((Fantasma) sprite).ultimaDir== Direccion.abajo){
//				this.mover(Direccion.abajo);
//				((Fantasma) sprite).mover(Direccion.arriba);
//			}
//		}

	}

	public void mover(Direccion dir) {

		if (dir == Direccion.arriba) {
			this.setVelocity(0, -5);
			this.ultimaDir = Direccion.arriba;
		} else if (dir == Direccion.izquierda) {
			this.setVelocity(-5, 0);
			this.ultimaDir = Direccion.izquierda;
		} else if (dir == Direccion.derecha) {
			this.setVelocity(5, 0);
			this.ultimaDir = Direccion.derecha;
		} else if (dir == Direccion.abajo) {
			this.setVelocity(0, 5);
			this.ultimaDir = Direccion.abajo;
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

}
