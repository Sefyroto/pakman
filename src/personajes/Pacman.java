package personajes;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import enums.Direccion;
import juego.MainGame;
import mapa.Bola;
import mapa.Pokeball;
import mapa.UI;

public class Pacman extends PhysicBody {

	public int vida;
	private static Pacman pacman = null;

	
	private Pacman(int x1, int y1, int x2, int y2, Field f) {
		super("Pacman", x1, y1, x2, y2, 0, "recursos/imagenes/pacman/pacman.gif", f);
		this.vida = 3;
	}
	
	public static Pacman instanciar(int x1, int y1, int x2, int y2, Field f) {
		if (pacman==null) {
			pacman = new Pacman(x1, y1, x2, y2, f);
		}
		return pacman;
		
	}

	public static Pacman instanciar() {
		return pacman;
	}
	
	@Override
	public void update() {
		// tp arriba y abajo
		if (this.x2 < 0) {
			this.x1 = 1920;
			this.x2 = 1970;
		} else if (this.x1 > 1920) {
			this.x1 = -50;
			this.x2 = 0;
		}
		// tp izquierda y derecha
		if (this.y2 < 0) {
			this.y1 = 1020;
			this.y2 = 1070;
		} else if (this.y1 > 1020) {
			this.y1 = -50;
			this.y2 = 0;
		}

	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if (sprite instanceof Fantasma) {
			if (((Fantasma) sprite).vulnerable == true) {
				Spawner.setFantasmas(-1);
				UI.getInstance().puntos.sumaPuntos(250);
				sprite.delete();
			} else {
				this.morir();
			}
		}

	}

	private void morir() {
		if (vida > 1) {
			this.vida--;
			this.x1 = 830;
			this.x2 = 880;
			this.y1 = 630;
			this.y2 = 680;
			UI.getInstance().vida.actualizarVida();
		} else {
			this.delete();
			this.vida--;
			UI.getInstance().vida.actualizarVida();
		}
	}
	
	@Override
	public void onTriggerEnter(Sprite sprite) {
		if (sprite instanceof Pokeball) {
			UI.getInstance().puntos.sumaPuntos(50);
			((Pokeball) sprite).destruir();
		} else if (sprite instanceof Bola) {
			UI.getInstance().puntos.sumaPuntos(10);
			((Bola) sprite).destruir();
			// System.out.println(exp);
		}

	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	public void mover(Direccion dir) {

		if (dir == Direccion.arriba) {
			this.setVelocity(0, -10);
			this.angle = 270;
		} else if (dir == Direccion.izquierda) {
			this.setVelocity(-10, 0);
			this.angle = 180;
		} else if (dir == Direccion.derecha) {
			this.setVelocity(10, 0);
			this.angle = 0;
		} else if (dir == Direccion.abajo) {
			this.setVelocity(0, 10);
			this.angle = 90;
		}

	}

	@Override
	public String toString() {
		return "Pacman [vida=" + vida + ", x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2 + "]";
	}

}
