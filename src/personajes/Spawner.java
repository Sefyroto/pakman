package personajes;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import enums.FantasmaColor;

public class Spawner {

	static int fantasmas = 0;
	int maxFantasmas = 3;
	static Field f;
	static Timer timer = new Timer();
	static TimerTask t;

	public Spawner(Field flocal) {
		f = flocal;
		iniciarTimer();
	}

	private void iniciarTimer() {
		t = new TimerTask() {
			@Override
			public void run() {
				if (fantasmas < maxFantasmas) {
					spawn();
				}
			
			}
		};
		timer.schedule(t, 2000, 2000);
	}

	private void spawn() {
		Random r = new Random();
		int random = r.nextInt(3);
		if (random==0) {
			new Fantasma (800, 525, 850, 575, FantasmaColor.ROJO, f);
		}else if (random==1) {
			new Fantasma (900, 525, 950, 575, FantasmaColor.VERDE, f);
		}else if (random==2) {
			new Fantasma (1000, 525, 1050, 575, FantasmaColor.ROSA, f);
		}
		
		
		
		
	}
	
	public static int getFantasmas() {
		return fantasmas;
	}
	
	public static void setFantasmas(int nfantasmas) {
		fantasmas += nfantasmas;		
	}

}
