package mapa;

import java.awt.Font;

import Core.Field;
import Core.Sprite;
import personajes.Pacman;

public class Vidas extends Sprite {

	public Vidas(int x1, int y1, Field f) {
		super("Vida", x1, y1, x1, y1, 0, "", f);
		this.solid = false;
		this.text = true;
		this.font = new Font("Arial", font.BOLD, 30);
		this.textColor = 0xFFFFFF;
		actualizarVida();
	}

	public void actualizarVida() {
		
		this.path="Vidas: "+ Pacman.instanciar().vida;
	}
}
