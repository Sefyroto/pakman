package mapa;

import java.awt.Font;

import Core.Field;
import Core.Sprite;
import personajes.Pacman;

public class Puntuacion extends Sprite {

	int puntos = 0;

	public Puntuacion(int x1, int y1, Field f) {
		super("Puntuacion", x1, y1, x1, y1, 0, "", f);
		this.solid = false;
		this.text = true;
		this.font = new Font("Arial", font.BOLD, 30);
		this.textColor = 0xFFFFFF;
		sumaPuntos(0);
	
	}

	public void sumaPuntos(int exp) {
		
		this.puntos += exp;
		this.path = "Puntuacion: " + this.puntos;
	}

}
