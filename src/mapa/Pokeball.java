package mapa;

import Core.Field;
import personajes.Fantasma;


public class Pokeball extends Bola {

	public Pokeball(int x1, int y1, int x2, int y2, Field f) {
		super(x1, y1, x2, y2, f);
		this.path="recursos/imagenes/Pelotas/pball.png";
	}
	
	@Override
	public void destruir() {
		Fantasma.comestible(f);
		this.delete();
	}
}
