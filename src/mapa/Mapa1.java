package mapa;

import Core.Field;
import enums.FantasmaColor;
import personajes.Fantasma;
import personajes.Spawner;

public class Mapa1 {
	
	public static void mapa1(Field f) {
		// arriba
		new Pared(0, 0, 900, 10, 0, f);
		new Pared(1000, 0, 1900, 10, 0, f);
		// isquierda
		new Pared(0, 0, 10, 500, 0, f);
		new Pared(0, 600, 10, 1010, 0, f);
		// derexa
		new Pared(1890, 0, 1900, 500, 0, f);
		new Pared(1890, 600, 1900, 1010, 0, f);
		// abaho
		new Pared(0, 1010, 900, 1020, 0, f);
		new Pared(1000, 1010, 1920, 1020, 0, f);

		// anillo exterior superior
		new Pared(100, 100, 450, 110, 0, f);
		new Pared(550, 100, 1300, 110, 0, f);
		new Pared(1400, 100, 1800, 110, 0, f);
		// anillo exterior izquierda
		new Pared(100, 100, 110, 500, 0, f);
		new Pared(100, 600, 110, 910, 0, f);
		// anillo exterior derecha
		new Pared(1800, 100, 1810, 500, 0, f);
		new Pared(1800, 600, 1810, 910, 0, f);
		// anillo exterior abajo
		new Pared(100, 900, 700, 910, 0, f);
		new Pared(800, 900, 1000, 910, 0, f);
		new Pared(1100, 900, 1800, 910, 0, f);
		// anillo intermedio superior
		new Pared(200, 200, 700, 210, 0, f);
		new Pared(800, 200, 1000, 210, 0, f);
		new Pared(1100, 200, 1700, 210, 0, f);
		// anillo intermedio izquierda
		new Pared(200, 200, 210, 300, 0, f);
		new Pared(200, 400, 210, 600, 0, f);
		new Pared(200, 700, 210, 800, 0, f);
		// anillo intermedio inferior
		new Pared(200, 800, 500, 810, 0, f);
		new Pared(600, 800, 850, 810, 0, f);
		new Pared(950, 800, 1200, 810, 0, f);
		new Pared(1300, 800, 1700, 810, 0, f);
		// anillo intermedio derecha
		new Pared(1700, 200, 1710, 300, 0, f);
		new Pared(1700, 400, 1710, 600, 0, f);
		new Pared(1700, 700, 1710, 810, 0, f);
		// anillo interior superior
		new Pared(300, 300, 450, 310, 0, f);
		new Pared(550, 300, 1300, 310, 0, f);
		new Pared(1400, 300, 1610, 310, 0, f);
		// anillo interior izquierda
		new Pared(300, 300, 310, 450, 0, f);
		new Pared(300, 550, 310, 700, 0, f);
		// anillo interior inferior
		new Pared(300, 700, 500, 710, 0, f);
		new Pared(600, 700, 1200, 710, 0, f);
		new Pared(1300, 700, 1610, 710, 0, f);
		// anillo interior derecha
		new Pared(1600, 300, 1610, 450, 0, f);
		new Pared(1600, 550, 1610, 700, 0, f);
		// anillo interno interno fantasmas superior
	//	new Pared(760, 400, 1150, 410, 0, f);
		// anillo interno interno fantasmas izquierda
		new Pared(400, 400, 410, 600, 0, f);
		new Pared(760, 400, 770, 600, 0, f);
		// anillo interno interno fantasmas inferior
		new Pared(400, 600, 1500, 610, 0, f);
		// anillo interno interno fantasmas derecha
		new Pared(1500, 400, 1510, 610, 0, f);
		new Pared(1150, 400, 1160, 610, 0, f);
		// BOLAS

		int y1 = 40;
		for (int i = 0; i < 10; i++) {
			int x1 = 50;
			for (int j = 0; j < 20; j++) {
				if (!((i == 4 || i == 5) && (j == 8 || j == 9 || j == 10 || j == 11))) {
					new Bola(x1, y1, x1 + 25, y1 + 25, f);
				}
				x1 += 94;
			}
			y1 += 100;
		}
		//Pokebolas de poder
		new Pokeball(30, 30, 80, 80, f);
		new Pokeball(30, 930, 80, 980, f);
		new Pokeball(1825, 30, 1875, 80, f);
		new Pokeball(1825, 930, 1875, 980, f);

		//Fantasmas
		new Fantasma (800, 525, 850, 575, FantasmaColor.ROJO, f);
		new Fantasma (900, 525, 950, 575, FantasmaColor.VERDE, f);
		new Fantasma (1000, 525, 1050, 575, FantasmaColor.ROSA, f);
		
		
	}
	

}
