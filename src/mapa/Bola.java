package mapa;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import personajes.Pacman;

public class Bola extends PhysicBody {

	public Bola(int x1, int y1, int x2, int y2, Field f) {
		super("Bola", x1, y1, x2, y2, 0, "recursos/imagenes/Pelotas/ball.png", f);

		this.trigger = true;
	}

	public void destruir() {
		this.delete();
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
//		if (sprite instanceof Pacman) {
//			this.delete();
//		}

	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

}
